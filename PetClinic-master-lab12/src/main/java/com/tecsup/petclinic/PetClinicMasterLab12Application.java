package com.tecsup.petclinic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 
 * @author jgomezm
 *
 */
@SpringBootApplication
public class PetClinicMasterLab12Application {

	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(PetClinicMasterLab12Application .class, args);
	}

}

