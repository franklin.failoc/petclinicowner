package com.tecsup.petclinic.domain;

import org.springframework.stereotype.Repository;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

@Repository
public interface OwnerRepository 
    extends CrudRepository<Owner, Long>{

    // Fetch pets by name
    List<Owner> findByFirstName(String firstName);

    // Fetch pets by typeId
    List<Owner> findByTelephone(String Telephone);

    // Fetch pets by typeId
    List<Owner> findByCity(String City);

}
