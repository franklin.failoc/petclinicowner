package com.tecsup.petclinic.service;

import java.util.List;

import com.tecsup.petclinic.domain.Owner;
import com.tecsup.petclinic.exception.OwnerNotFoundException;
import com.tecsup.petclinic.exception.PetNotFoundException;

/**
 * 
 * @author Samuel
 *
 */

public interface OwnerService {

		/**
		 * 
		 * @param owner
		 * @return
		 */
		Owner create(Owner owner);

		/**
		 * 
		 * @param owner
		 * @return
		 */
		Owner update(Owner owner);

		/**
		 * 
		 * @param id
		 * @throws PetNotFoundException
		 */
		void delete(Long id) throws OwnerNotFoundException;

		/**
		 * 
		 * @param id
		 * @return
		 */
		Owner findById(long id) throws OwnerNotFoundException;

		/**
		 * 
		 * @param ownerName
		 * @return
		 */
		List<Owner> findByfirst_name(String ownerName);

		/**
		 * 
		 * @param ownerCity
		 * @return
		 */
		List<Owner> findBycity(String ownerCity);

		/**
		 * 
		 * @param ownerTelephone
		 * @return
		 */
		List<Owner> findBytelephone(String ownerTelephone);

		/**
		 * 
		 * @return
		 */
		Iterable<Owner> findAll();

	

}

