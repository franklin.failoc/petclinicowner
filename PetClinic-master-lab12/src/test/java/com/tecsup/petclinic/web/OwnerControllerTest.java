package com.tecsup.petclinic.web;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import com.tecsup.petclinic.domain.Owner;

/**
 * 
 */
@AutoConfigureMockMvc
@SpringBootTest
public class OwnerControllerTest {


	private static final ObjectMapper om = new ObjectMapper();

	@Autowired
	private MockMvc mockMvc;


	@Test
	public void testGeOwners() throws Exception {

		//int NRO_RECORD = 73;
		int ID_FIRST_RECORD = 1;

		this.mockMvc.perform(get("/owners"))
		.andExpect(status().isOk())
		.andExpect(content()
				.contentType(MediaType.APPLICATION_JSON))
		//		    .andExpect(jsonPath("$", hasSize(NRO_RECORD)))
		.andExpect(jsonPath("$[0].id", is(ID_FIRST_RECORD)));
	}


	/**
	 * 
	 * @throws Exception
	 */
	@Test
	public void testFindOwnersOK() throws Exception {

		String FIRST_NAME = "George";
		String lAST_NAME = "Franklin";
		String ADDRESS ="110 W. Liberty St.";
		String CITY ="Madison";
		String TELEPHONE="6085551023";

		mockMvc.perform(get("/owners/1"))  // Object must be BASIL 
		.andExpect(content().contentType(MediaType.APPLICATION_JSON))
		//.andDo(print())
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.id", is(1)))
		.andExpect(jsonPath("$.firstName", is(FIRST_NAME)))
		.andExpect(jsonPath("$.lastName", is(lAST_NAME)))
		.andExpect(jsonPath("$.address", is(ADDRESS)))
		.andExpect(jsonPath("$.city", is(CITY)))
		.andExpect(jsonPath("$.telephone", is(TELEPHONE)));
		
		
	}

	/**
	 * 
	 * @throws Exception
	 */
	@Test
	public void testFindOwnersKO() throws Exception {

		mockMvc.perform(get("/owners/666"))
		.andExpect(status().isNotFound());

	}

	/**
	 * 
	 * @throws Exception
	 */
	@Test
	public void testCreateOwners() throws Exception {

		String FIRST_NAME = "ssssss";
		String lAST_NAME = "Franklinss";
		String ADDRESS ="110 W. Liberty St.ss";
		String CITY ="Madisonss";
		String TELEPHONE="6085551023";

		Owner newOwner = new Owner(FIRST_NAME,lAST_NAME,ADDRESS,CITY,TELEPHONE);

		mockMvc.perform(post("/owners")
				.content(om.writeValueAsString(newOwner))
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
		.andDo(print())
		.andExpect(status().isCreated())
		//.andExpect(jsonPath("$.id", is(1)))
		.andExpect(jsonPath("$.firstName", is(FIRST_NAME)))
		.andExpect(jsonPath("$.lastName", is(lAST_NAME)))
		.andExpect(jsonPath("$.address", is(ADDRESS)))
		.andExpect(jsonPath("$.city", is(CITY)))
		.andExpect(jsonPath("$.telephone", is(TELEPHONE)));

	}

	/**
	 * 
	 * @throws Exception
	 */
	@Test
	public void testDeletePet() throws Exception {
		
		String FIRST_NAME = "ssssss";
		String lAST_NAME = "Franklinss";
		String ADDRESS ="110 W. Liberty St.ss";
		String CITY ="Madisonss";
		String TELEPHONE="6085551023";

		Owner newOwner = new Owner(FIRST_NAME,lAST_NAME,ADDRESS,CITY,TELEPHONE);

		ResultActions mvcActions = mockMvc.perform(post("/owners")
				.content(om.writeValueAsString(newOwner))
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isCreated());

		String response = mvcActions.andReturn().getResponse().getContentAsString();

		Integer id = JsonPath.parse(response).read("$.id");

		mockMvc.perform(delete("/owners/" + id ))
		/*.andDo(print())*/
		.andExpect(status().isOk());
	}




}
