package com.tecsup.petclinic.service;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.tecsup.petclinic.domain.Owner;
import com.tecsup.petclinic.exception.OwnerNotFoundException;



@SpringBootTest
public class OwnerServiceTest {


	private static final Logger logger = LoggerFactory.getLogger(OwnerServiceTest.class);

	@Autowired
	private OwnerService ownerService;

	/**
	 * 
	 */
	@Test
	public void testFindOwnerById() {

		long ID = 1;
		String first_name = "George";
		Owner owner = null;

		try {

			owner = ownerService.findById(ID);

		} catch (OwnerNotFoundException e) {
			assertThat(e.getMessage(),false);
		}
		logger.info("" + owner);

		assertThat(first_name,is( owner.getFirstName()));

	}

	/**
	 * 
	 */
	@Test
	public void testFindOwnerByFirst_name() {

		String first_name = "George";
		int SIZE_EXPECTED = 1;

		List<Owner> owners = ownerService.findByfirst_name(first_name);

		assertThat(SIZE_EXPECTED,is( owners.size()));
	}

	/**
	 * 
	 */
	@Test
	public void testFindOwnerByCity() {

		String CITY = "Madison";
		int SIZE_EXPECTED = 4;

		List<Owner> owners = ownerService.findBycity(CITY);

		assertThat(SIZE_EXPECTED,is( owners.size()));
	}

	/**
	 * 
	 */
	@Test
	public void testFindPetByTelephone() {

		String TELEPHONE = "6085551023";
		int SIZE_EXPECTED = 2;

		List<Owner> owners = ownerService.findBytelephone(TELEPHONE);

		assertThat(SIZE_EXPECTED,is( owners.size()));
	}

	/**
	 *  To get ID generate , you need 
	 *  setup in id primary key in your
	 *  entity this annotation :
	 *  	@GeneratedValue(strategy = GenerationType.IDENTITY)
	 */
	@Test
	public void testCreateOwner() {

		String FIRST_NAME = "samuel";
		String lAST_NAME = "altamirano";
		String ADDRESS ="san Andres";
		String CITY ="trujillo";
		String TELEPHONE="902333952";

		Owner owner = new Owner(FIRST_NAME,lAST_NAME,ADDRESS,CITY,TELEPHONE);
		owner = ownerService.create(owner);
		logger.info("" + owner);

		assertThat(owner.getId(), notNullValue());
		assertThat(FIRST_NAME,is( owner.getFirstName()));
		assertThat(lAST_NAME,is( owner.getLastName()));
		assertThat(ADDRESS,is( owner.getAddress()));
		assertThat(CITY,is( owner.getCity()));
		assertThat(TELEPHONE,is( owner.getTelephone()));

	}

	/**
	 * 
	 */
	@Test
	public void testUpdateOwner() {

		String FIRST_NAME = "juan";
		String lAST_NAME = "chaves";
		String ADDRESS ="san vernardo";
		String CITY ="trujillo";
		String TELEPHONE="902231546";
		long create_id = -1;

		String UP_FIRST_NAME = "juana";
		String UP_lAST_NAME = "dominges";
		String UP_ADDRESS ="san juanito";
		String UP_CITY ="trujillo";
		String UP_TELEPHONE="902231540";

		Owner owner = new Owner(FIRST_NAME,lAST_NAME,ADDRESS,CITY,TELEPHONE);

		// Create record
		logger.info(">" + owner);
		Owner readOwner = ownerService.create(owner);
		logger.info(">>" + readOwner);

		create_id = readOwner.getId();

		// Prepare data for update
		readOwner.setFirstName(UP_FIRST_NAME);
		readOwner.setLastName(UP_lAST_NAME);;
		readOwner.setAddress(UP_ADDRESS);
		readOwner.setCity(UP_CITY);
		readOwner.setTelephone(UP_TELEPHONE);

		// Execute update
		Owner upgradeOwner = ownerService.update(readOwner);
		logger.info(">>>>" + upgradeOwner);

		assertThat(create_id , notNullValue());
		assertThat(create_id,is( upgradeOwner.getId()));
		assertThat(UP_FIRST_NAME,is( upgradeOwner.getFirstName()));
		assertThat(UP_lAST_NAME,is( upgradeOwner.getLastName()));
		assertThat(UP_ADDRESS,is( upgradeOwner.getAddress()));
		assertThat(UP_CITY,is( upgradeOwner.getCity()));
		assertThat(UP_TELEPHONE,is( upgradeOwner.getTelephone()));
	}

	/**
	 * 
	 */
	@Test
	public void testDeleteOwner() {

		String FIRST_NAME = "del";
		String lAST_NAME = "late";
		String ADDRESS ="san judas";
		String CITY ="trujillo";
		String TELEPHONE="902333900";

		Owner owner = new Owner(FIRST_NAME,lAST_NAME,ADDRESS,CITY,TELEPHONE);
		owner = ownerService.create(owner);
		logger.info("" + owner);

		try {
			ownerService.delete(owner.getId());
		} catch (OwnerNotFoundException e) {
			assertThat(e.getMessage(),false);
		}
		
		try {
			ownerService.findById(owner.getId());
			assertThat(true, is(false));
		} catch (OwnerNotFoundException e) {
			assertThat(true, is(true));
		} 				

	}



}
